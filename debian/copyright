Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: LiVES
Upstream-Contact: Gabriel Finch <salsaman@gmail.com>
Source: http://www.xs4all.nl/%7Esalsaman/lives/current/

Files: *
Copyright:
  2002-2017 Gabriel Finch <salsaman@gmail.com>
  2002 W.P. van Paassen <peter@paassen.tmfweb.nl>
  2010 A. Penkov
  2001-2005 FUKUCHI Kentarou
  2004-2005 project authors
License: GPL-3+

Files: libOSC/*
Copyright: 1992-2004 The Regents of the University of California (Regents)
License: libosc-1

Files:
 src/giw/*
 lives-plugins/plugins/decoders/mkv_decoder.*
 lives-plugins/plugins/decoders/flv_decoder.*
 lives-plugins/plugins/decoders/asf_decoder.*
 lives-plugins/plugins/decoders/mpegts_decoder.*
 lives-plugins/plugins/decoders/libav_helper.h
Copyright:
  2011-2014 G. Finch <salsaman@gmail.com>
  2006 Alexandre Pereira Bueno, Eduardo Parente Ribeiro
  2003 The FFmpeg Project
  2000-2003 Fabrice Bellard
License: LGPL-2.1+

Files: intl/*
Copyright: 1995-2007 Free Software Foundation, Inc
License: LGPL-2+

Files:
 libweed/*
 lives-plugins/weed-plugins/weed-plugin-utils.c
 lives-plugins/weed-plugins/weed-utils-code.c
 src/giw/giwtimeline.c
 src/giw/giwtimeline.h
Copyright:
  2005-2010 Gabriel "Salsaman" Finch
  1995-1997, Peter Mattis and Spencer Kimball
License: LGPL-3+

Files: lives-plugins/weed-plugins/farneback_analyser.cpp
Copyright: G. Finch (salsaman) 2012
License: BSD-2-clause

Files:
 src/colourspace.c
 lives-plugins/weed-plugins/gdk/haar_analyser.*
Copyright:
  2003, Ricardo Niederberger Cabral
  2004-2013 G. Finch (salsaman)
License: GPL-2+

Files: lives-plugins/marcos-encoders/*
Copyright: 2004-2005 Marco De la Cruz
License: GPL-1+

Files: src/osc.h
Copyright:
  1992-2004 The Regents of the University of California (Regents)
  2003-2012 G. Finch
License: GPL-3+
Comment: Some parts are based on libOSC, see the
 libosc-1 license.

Files: lives-plugins/icons/*
Copyright: 2009 Carles Carbonell Bernado
License: LGPL-3+

Files: debian/*
Copyright:
  2010-2014 Alessio Treglia <alessio@debian.org>
  2007-2009 Gürkan Sengün <gurkan@phys.ethz.ch>
  2009-2010 Harry Rickards <hrickards@l33tmyst.com>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-3'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: GPL-1+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-1'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-2.1+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in `/usr/share/common-licenses/LGPL-2'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: libosc-1
 Permission to use, copy, modify, distribute, and distribute modified versions
 of this software and its documentation without fee and without a signed
 licensing agreement, is hereby granted, provided that the above copyright
 notice, this paragraph and the following two paragraphs appear in all copies,
 modifications, and distributions.
 .
 IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
 SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING
 OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF REGENTS HAS
 BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED
 HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE
 MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.
